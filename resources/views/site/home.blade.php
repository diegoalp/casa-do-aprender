@extends('site.parts.base')
@section('content')

<div class="row slideshow">
        <div id="carouselExampleControls" class="carousel slide w-100" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                  <img class="d-block w-100" src="{{ asset('/imagens/slide1.jpg') }}" alt="First slide">
                  <div class="carousel-caption d-none d-md-block">
                    <h3>Reforço escolar</h3>
                    <p>do 1º ao 5º ano</p>
                  </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('/imagens/slide2.jpg') }}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                      <h3>Educação Infantil</h3>
                      <p>ESCOLA - PARCIAL - INTEGRAL</p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('/imagens/slide3.jpg') }}" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                      <h3>arte na educação infantil</h3>
                      <p>Fundamental para o desenvolvimento da criança</p>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Próximo</span>
                </a>
              </div>
</div>
<div class="container destaque">
    <h1 class="text-center">Excelência no cuidado e educação</h1>
    <h2 class="text-center"><span style="color: #d048f3;">do berçario ao infantil II. A melhor opção pro <strong>futuro</strong> do seu filho.</span></h2>
    <p class="text-center">Contamos com equipe qualificada no atendimento à bebês a partir dos 4 meses, que participa de formação continuada ao longo de todo o ano letivo e supervisão pedagógica permanente.</p>
    <div class="row diferenciais">
        <div class="col-md-4">
            <a href="#" class="stretched-link">
              <img class="w-100" src="{{ asset('/imagens/banner-ingles.jpg') }}">
            </a>  
            <div class="bg-azul">
                <h2>Inglês</h2>
                <p>Aulas de iniciação à língua inglesa. As aulas são dinâmicas, facilitando o aprendizado, fazendo com que o aluno absorva muito mais fácil.</p>
            </div>
        </div>
        <div class="col-md-4">
            <a href="#" class="stretched-link">
              <img class="w-100" src="{{ asset('/imagens/banner-ballet.jpg') }}">
            </a>  
            <div class="bg-verde">
                <h2>Ballet</h2>
                <p>O Ballet ajuda a desenvolver os aspectos de postura, motores, cognitivos,  imaginação, afetivo-sociais, entre outros.</p>
            </div>
        </div>
        <div class="col-md-4">
            <a href="#" class="stretched-link">
              <img class="w-100" src="{{ asset('/imagens/banner-bercario.jpg') }}">
            </a>  
            <div class="bg-rosa">
                <h2>Berçário</h2>
                <p>O planejamento de estimulação pedagógica é realizado de maneira individual para cada criança, desde a sua vivência e exploração ou em grupo de acordo com a fase e necessidades de cada criança.</p>
            </div>
        </div>
    </div>
</div>
<div class="row cta cta_1">
  <div class="col-md-4"><h1 class="text-right">MUITO + QUE UMA ESCOLA<!/h1></div>
  <div class="col-md-5"><p class="text-center">Além do desenvolvimento pedagógico, oferecemos os cuidados e segurança que seu filho precisa.</p></div>
  <div class="col-md-3"><button class="btn btn-outline-light btn-lg">MATRÍCULAS</button></div>
</div>
<div class="row icones">
      <div class="col-md-4 row">
        <div class="col-md-9 text-right">
          <h3>Lugar agradável e divertido</h3>
        </div>
        <div class="col-md-3"><div class="icone icone-azul"><i class="fa fa-calendar"></i></div></div>
        <div class="col-md-9 text-right">
          <h3>Monitoramento online</h3>
        </div>
        <div class="col-md-3"><div class="icone icone-vermelho"><i class="fa fa-video-camera"></i></div></div>
      </div>
      <div class="col-md-4"><img class="w-100" src="{{ asset('/imagens/welcome-2.png') }}"></div>
      <div class="col-md-4 row">
          <div class="col-md-3"><div class="icone icone-rosa"><i class="fa fa-group"></i></div></div>
        <div class="col-md-9 text-left">
          <h3>Profissionais qualificados</h3>
        </div>
        <div class="col-md-3"><div class="icone icone-verde"><i class="fa fa-paper-plane-o"></i></div></div>
        <div class="col-md-9 text-left">
          <h3>Comprometimento</h3>
        </div>
      </div>
</div> 
{{-- <div class="container">
    <div class="row noticias">
      <div class="col-md-4">
          <div class="card">
              <img src="{{ asset('/imagens/noticia1.jpg') }}" class="card-img-top" height="200">
              <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <p align="center"><a href="#" class="stretched-link btn btn-warning btn-lg">Mais</a></p>
              </div>
          </div>
      </div>
      <div class="col-md-4">
          <div class="card">
              <img src="{{ asset('/imagens/noticia2.jpg') }}" class="card-img-top" height="200">
              <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <p align="center"><a href="#" class="stretched-link btn btn-warning btn-lg">Mais</a></p>
              </div>
          </div>
      </div>
      <div class="col-md-4">
          <div class="card">
              <img src="{{ asset('/imagens/noticia3.jpg') }}" class="card-img-top" height="200">
              <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <p align="center"><a href="#" class="stretched-link btn btn-warning btn-lg">Mais</a></p>
              </div>
          </div>
      </div>
  </div>
</div> --}}
<div class="row qdo_negro">
  <div class="container">
    <h1>We are group of teachers who really love</h1>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    <p align="center"><button type="button" class="btn btn-primary btn-lg">Large button</button></p>
  </div>
</div>
<div class="container professores">
    <h1 class="text-center">Profissionais</h1>
    <h2 class="text-center"><span style="color: #d048f3;">os melhores para um <strong>FUTURO</strong></span> melhor.</h2>
    <p class="text-center">Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis.</p>
  <div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
              <img src="{{ asset('/imagens/professor1.jpg') }}" alt="..." class="w-50 rounded-circle mx-auto d-block">
              <h5 class="card-title text-center">João Souza</h5>
              <h6 class="card-subtitle mb-2 text-muted text-center">Card subtitle</h6>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
          </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
              <img src="{{ asset('/imagens/professor2.jpg') }}" alt="..." class="w-50 rounded-circle mx-auto d-block">
              <h5 class="card-title text-center">Marina Santos</h5>
              <h6 class="card-subtitle mb-2 text-muted text-center">Card subtitle</h6>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
          </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <img src="{{ asset('/imagens/professor3.jpg') }}" alt="..." class="w-50 rounded-circle mx-auto d-block">
              <h5 class="card-title text-center">Lettícia Miller</h5>
              <h6 class="card-subtitle mb-2 text-muted text-center">Card subtitle</h6>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
          </div>
    </div>
  </div>
</div>
@endsection