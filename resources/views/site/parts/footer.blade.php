<div class="row footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                    <a href="/"><img src="{{ asset('/imagens/tema/logo_casa_do_Aprender_rodape.png') }}" width="200" style="margin-bottom: 10px;"></a>
                <p>Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis.</p>
            </div>
            <div class="col-md-3">
                <h3>ACESSE</h3>
                    <ul class="list">
                            <li><a href="#">Instalações</a></li>
                            <li><a href="#">Turmas</a></li>
                            <li><a href="#">Fotos</a></li>
                            <li><a href="#">Atividades</a></li>
                            <li><a href="#">Calendário</a></li>
                        </ul>
            </div>
            <div class="col-md-3">
                    <h3>ESCOLA</h3>
                    <ul class="list">
                            <li><a href="#">Inglês</a></li>
                            <li><a href="#">Ballet</a></li>
                            <li><a href="#">Bercário</a></li>
                            <li><a href="#">Professores</a></li>
                            <li><a href="#">Diferenciais</a></li>
                        </ul>
            </div>
            <div class="col-md-3">
                <p>(98) 9.9105-7863</p>
                <p>Rua 96, qd 75, N 11 - Vinhais - São Luis - Ma - CEP 65.074-680</p>
                <div class="row">
                        <ul class="social-links-two">
                                <li class="facebook"><a href="#"><span class="fa fa-facebook"></span></a></li>
                                <li class="instagram"><a href="#"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                </div>
            </div>
            <div class="container copyright">
                    <div class="row">
                        © Copyrights 2019 A Casa do Aprender. Todos os direitos reservados.
                    </div>
            </div>
        </div>
    </div>
</div>