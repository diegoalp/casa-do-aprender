<div class="row menu">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-5">
                    <ul class="nav justify-content-end">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Instalações</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Fotos</a>
                            </li>
                    </ul>
            </div>
        <div class="col-md-2"><a href="/"><img src="{{ asset('/imagens/tema/logo_casa_do_Aprender.png') }}" width="100%"></a></div>
            <div class="col-md-5">
                    <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Matrículas</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#">Fale Conosco</a>
                            </li>
                    </ul>
            </div>
        </div>
    </div>
</div>